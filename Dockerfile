FROM node:lts-bookworm-slim

WORKDIR /app

ARG BW_VERSION

RUN npm i -g @bitwarden/cli@${BW_VERSION} && \
    npm cache clean --force

COPY export.sh /app

ENV client_id_bitwarden=${client_id_bitwarden}
ENV client_secret_bitwarden=${client_secret_bitwarden}
ENV masterpassword_bitwarden=${masterpassword_bitwarden}

ENTRYPOINT ["/bin/bash", "export.sh"]