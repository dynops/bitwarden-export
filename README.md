# Export Vault Bitwarnden

## Présentation
Ce Repository mets à disposition une image Docker vous permettant d'exporter votre coffre-fort Bitwarden en un fichier `Json`.

L'image Docker exécute un script [export.sh](export.sh) qui effectue les actions suivantes :

- Synchronisation de la CLI de bitwarden vers le serveur `Bitwarden.com` (afin d'avoir une version du coffre-fort à jour)
- Export `Json` du coffre-fort Bitwarden
- Garde 14 jours de rétention de l'export json en supprimant tous les fichiers export-bitwarden-*.json dans le répertoire monté en volume

### Compatibilité
L'image Docker a été build pour être compatible avec `ARM`. Vous pourrez donc utiliser ce projet pour votre Raspberry Pi !

## Prérequis

- Récupérer le **client_id**, le **client_secret** en allant dans "View API Key" sur son vault bitwarden ainsi que son **mot de passe maître**.

### Etapes
1. Aller dans les clés de son compte Bitwarden
![](doc/img/key.png)   
2. Cliquer sur "Voir les clés API" et récupérer le **client_id**, le **client_secret**
![](doc/img/view-api-key.png)

## Example

### Voici un exemple d'export généré :
![](doc/img/export-generated.png)

## Docker
### Docker-compose
Modifier les variables d'environnements dans le fichier [.env](.env) :
```bash
# Credentials
client_id_bitwarden=CLIENT_ID
client_secret_bitwarden=CLIENT_SECRET
masterpassword_bitwarden=MASTER_PASSWORD

# Directory of backup destination
VOLUME_DEST_EXPORT=YOUR_DIRECTORY_DEST
```
## Déploiement
```bash
git clone https://gitlab.com/dynops/bitwarden-export.git
cd bitwarden-export
docker-compose up
```

### Docker Run
```bash
docker run --rm -td \
-e client_id_bitwarden="CLIENT_ID" \
-e client_secret_bitwarden="CLIENT_SECRET" \
-e masterpassword_bitwarden='MASTER_PASSWORD' \
-v <VOLUME_DEST_EXPORT>:/export \
anomyn/bw-export
```

# References
Article de DynOps : https://dynops.fr/bitwarden-export