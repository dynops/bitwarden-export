#!/bin/bash

# Connexion
(echo $client_id_bitwarden ; sleep 5 ; echo $client_secret_bitwarden) | bw login --apikey

# Synchro du bw-cli avec le serveur bitwarden.com
echo $masterpassword_bitwarden | bw sync

date_today=$(date +"%d-%m-%y")

(echo $masterpassword_bitwarden ; sleep 3 ; echo $masterpassword_bitwarden) | bw export --output /export/export-bitwarden-$date_today.json --format json

bw logout

# Suppression des fichiers datant de plus de 14 jours
find /export/export-bitwarden-*.json -type f -mtime +14 -delete